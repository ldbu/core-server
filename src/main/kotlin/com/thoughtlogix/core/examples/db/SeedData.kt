/*
 * MIT License
 *
 * Copyright (c) 2016 Thought Logix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.thoughtlogix.core.examples.db

import com.thoughtlogix.core.examples.models.Todo
import com.thoughtlogix.core.server.db.JPA
import com.thoughtlogix.core.server.models.users.Role
import com.thoughtlogix.core.server.models.users.Team
import com.thoughtlogix.core.server.models.users.User
import com.thoughtlogix.core.server.services.db.GenericDbService
import org.slf4j.LoggerFactory

class SeedData(val jpa: JPA) {

    val logger = LoggerFactory.getLogger(SeedData::class.java)

    fun loadData(includeFake: Boolean) {

        val count = (jpa.entityManager!!.createQuery("select count(u.id) from User u").singleResult as Long).toInt()
        if (count == 0) {
            loadSeedData()
        }
    }

    private fun loadSeedData() {

        logger.info("Importing Seed Data")

        var genericService: GenericDbService
        jpa?.beginTransaction()

        ////////////////////////////////////////////////////////////////////
        // Permissions
        ////////////////////////////////////////////////////////////////////
        //        logger.info("Importing Permissions...");
        //        genericService = new GenericService(jpa, Permission.class);
        //        Permission defaultPermission = new Permission();
        //        defaultPermission.setName("Default Permission");
        //        genericService.save(defaultPermission);

        ////////////////////////////////////////////////////////////////////
        // Roles
        ////////////////////////////////////////////////////////////////////
        logger.info("Importing Roles...")

        //
        // Generic
        //
        genericService = GenericDbService(jpa, Role::class.java)
        val adminRole = Role()
        adminRole.name = "Admin"
        genericService.save(adminRole)

        val userRole = Role()
        userRole.name = "User"
        genericService.save(userRole)


        ////////////////////////////////////////////////////////////////////
        // Teams
        ////////////////////////////////////////////////////////////////////
        logger.info("Importing Teams...")

        //
        // Default
        //
        genericService = GenericDbService(jpa, Team::class.java)
        val defaultTeam = Team()
        defaultTeam.name = "Default"
        defaultTeam.isDefaultDeny = false
        genericService.save(defaultTeam)

        ////////////////////////////////////////////////////////////////////
        // Users
        ////////////////////////////////////////////////////////////////////
        logger.info("Importing Users...")

        //
        // Defaults
        //

        val user = User()
        user.isActive = true
        user.username = "user"
        user.password = "user-pass"
        user.email = "support@example.com"
        user.roles = setOf(userRole)
        user.teams = setOf(defaultTeam)
        user.notes = "This is the default user."
        genericService.save(user)

        val adminUser = User()
        adminUser.isActive = true
        adminUser.username = "admin"
        adminUser.password = "admin-pass"
        adminUser.email = "support@example.com"
        adminUser.roles = setOf(userRole, adminRole)
        adminUser.teams = setOf(defaultTeam)
        adminUser.notes = "This is the admin user."
        genericService.save(adminUser)

        jpa?.commitTransaction()
    }
}
