/*
 * MIT License
 *
 * Copyright (c) 2016 Thought Logix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.thoughtlogix.core.server.controllers.account

import com.thoughtlogix.core.server.Lang
import com.thoughtlogix.core.server.controllers.Controller
import com.thoughtlogix.core.server.db.JPA
import com.thoughtlogix.core.server.models.core.Profile
import com.thoughtlogix.core.server.models.forms.ChangePasswordForm
import com.thoughtlogix.core.server.models.users.User
import com.thoughtlogix.core.server.other.htmlOut
import com.thoughtlogix.core.server.other.parseInput
import com.thoughtlogix.core.server.other.serializedOut
import com.thoughtlogix.core.server.services.db.LogDbService
import com.thoughtlogix.core.server.services.db.UserDbService
import com.thoughtlogix.core.server.utils.Crypto
import org.slf4j.LoggerFactory
import spark.Spark
import spark.Spark.get

class AccountController(jpa: JPA) : Controller(jpa) {
    override val logger = LoggerFactory.getLogger(AccountController::class.java)
    private val service: UserDbService

    init {
        service = UserDbService(jpa)
        logService = LogDbService(jpa)
        basePath = "/account"

        initCommonFilters(arrayOf<String>("/account", "/account/*"), "user")

        get(basePath + "/dashboard") { req, res ->
            val u = model.get("user") as User
            val user = service.getByUserName(u.username)
            if (isHtml(req)) {
                if (user != null) {
                    model.put("item", user)
                }
                return@get htmlOut(model, basePath + "/dashboard.peb")
            } else {
                return@get serializedOut(user, getFormat(req))
            }
        }

        get(basePath + "") { req, res ->
            val u = model.get("user") as User
            val user = service.getByUserName(u.username)
            if (isHtml(req)) {
                if (user != null) {
                    model.put("item", user)
                }
                return@get htmlOut(model, basePath + "/index.peb")
            } else {
                return@get serializedOut(user, getFormat(req))
            }
        }

        get(basePath + "/history") { req, res ->
            val u = model.get("user") as User
            val pageParams = getPageParams(req, res)
            pageParams.filter = u.username
            pageParams.order = "createdAt"
            pageParams.sort = "desc"
            val pagedData = logService.getMyPagedData(pageParams)
            model.put("pageParams", pageParams)
            if (isHtml(req)) {
                model.put("pagedData", pagedData)
                return@get htmlOut(model, basePath + "/history.peb")
            } else {
                return@get serializedOut(pagedData, getFormat(req))
            }
        }


        get(basePath + "/settings") { req, res ->
            val u = model.get("user") as User
            val settings = service.getByUserName(u.username)!!.settings
            if (isHtml(req)) {
                if (settings != null) {
                    model.put("item", settings)
                }
                return@get htmlOut(model, basePath + "/settings.peb")
            } else {
                return@get serializedOut(settings, getFormat(req))
            }
        }

        Spark.post(basePath + "/settings") { req, res ->
            val body = req.body()
            val u = model.get("user") as User
            val user = service.getByUserName(u.username)
            user!!.settings = body
            model.put("item", service.save(user))
            if (isHtml(req)) {
                Lang.tr("crud.update.success", user.toString())
                return@post redirect(res, basePath + "/" + user.getId())
            } else {
                return@post serializedOut(user, getFormat(req))
            }
        }

        get(basePath + "/profile") { req, res ->
            val u = model.get("user") as User
            val profile = service.getByUserName(u.username)!!.profile
            if (isHtml(req)) {
                if (profile != null) {
                    model.put("item", profile)
                }
                return@get htmlOut(model, basePath + "/profile.peb")
            } else {
                return@get serializedOut(profile, getFormat(req))
            }
        }

        Spark.post(basePath + "/profile") { req, res ->
            val cls = Profile::class.java
            val u = model.get("user") as User
            val user = service.getByUserName(u.username)
            val profile = parseInput(req, cls, getFormat(req))
            //            profileServer.save(profile!!)
            if (profile != null) {
                model.put("item", profile)
            }
            user!!.profile = profile
            service.save(user)
            if (isHtml(req)) {
                Lang.tr("crud.update.success", user.toString())
                return@post redirect(res, basePath + "/profile")
            } else {
                return@post serializedOut(user, getFormat(req))
            }
        }

        get(basePath + "/changepassword") { req, res -> htmlOut(model, basePath + "/changepassword.peb") }

        Spark.post(basePath + "/changepassword") { req, res ->
            val changePasswordForm = parseInput(req, ChangePasswordForm::class.java, getFormat(req))
            val currentPassword = changePasswordForm!!.currentPassword
            val newPassword = changePasswordForm.newPassword
            val repeatPassword = changePasswordForm.repeatPassword


            if (currentPassword.isNullOrBlank() || newPassword.isNullOrBlank() || repeatPassword.isNullOrBlank()) {
                redirect(res, "/401")
            }

            //            Server.jpa!!.beginTransaction()
            val u = model.get("user") as User
            val user = service.getByUserName(u.username)

            if (user == null) {
                res.redirect("/404")
                Spark.halt()
            }

            if (!Crypto.validatePassword(currentPassword, user!!.password, user.seed) || newPassword != repeatPassword) {
                res.redirect("/401")
                Spark.halt()
            }

            user.password = newPassword
            service.save(user)
            //            Server.jpa!!.commitTransaction()

            flash.addInfo(model.get("sessionid") as String, Lang.tr("msg.user.passwordchanged"))

            redirect(res, "/account")
        }

    }
}