package com.thoughtlogix.core.server.db

import org.hibernate.HibernateException
import org.hibernate.c3p0.internal.C3P0ConnectionProvider
import org.hibernate.engine.config.spi.ConfigurationService
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider
import org.hibernate.service.spi.ServiceRegistryAwareService
import org.hibernate.service.spi.ServiceRegistryImplementor

import java.sql.Connection
import java.sql.SQLException

class MultiTenantProvider : MultiTenantConnectionProvider, ServiceRegistryAwareService {

    private var connectionProvider: C3P0ConnectionProvider? = null

    override fun supportsAggressiveRelease(): Boolean {
        return false
    }

    override fun injectServices(serviceRegistry: ServiceRegistryImplementor) {
        val lSettings = serviceRegistry.getService(ConfigurationService::class.java).settings

        connectionProvider = C3P0ConnectionProvider()
        connectionProvider!!.injectServices(serviceRegistry)
        connectionProvider!!.configure(lSettings)
    }

    override fun isUnwrappableAs(clazz: Class<*>): Boolean {
        return false
    }

    override fun <T> unwrap(clazz: Class<T>): T? {
        return null
    }

    @Throws(SQLException::class)
    override fun getAnyConnection(): Connection {
        return connectionProvider!!.connection
    }

    @Throws(SQLException::class)
    override fun getConnection(tenantIdentifier: String): Connection {
        val connection = anyConnection
        try {
            connection.createStatement().execute("SET search_path to " + tenantIdentifier)
        } catch (e: SQLException) {
            throw HibernateException("Could not alter JDBC connection to specified schema [$tenantIdentifier]", e)
        }

        return connection
    }

    @Throws(SQLException::class)
    override fun releaseAnyConnection(connection: Connection) {
        try {
            connection.createStatement().execute("SET search_path to public")
        } catch (e: SQLException) {
            throw HibernateException("Could not alter JDBC connection to specified schema [s2]", e)
        }

        connectionProvider!!.closeConnection(connection)
    }

    @Throws(SQLException::class)
    override fun releaseConnection(tenantIdentifier: String, connection: Connection) {
        releaseAnyConnection(connection)
    }

    companion object {

        private val serialVersionUID = 4368575201221677384L
    }
}