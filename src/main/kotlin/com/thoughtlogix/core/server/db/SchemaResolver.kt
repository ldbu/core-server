package com.thoughtlogix.core.server.db

import com.thoughtlogix.core.server.controllers.Controller
import org.hibernate.context.spi.CurrentTenantIdentifierResolver

class SchemaResolver : CurrentTenantIdentifierResolver {

    override fun resolveCurrentTenantIdentifier(): String {
        try {
            return Controller.currentTenantName.get()
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    override fun validateExistingCurrentSessions(): Boolean {
        return false
    }
}